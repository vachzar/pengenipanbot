<?php
use Psr\Log\LoggerInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/', 'HomeController:index')->setName('root');

$app->get('/set', 'HomeController:setBot');

$app->get('/cron', 'HomeController:command');

$app->get('/unset', 'HomeController:unsetBot');

$app->post('/hook', 'HomeController:hook');
