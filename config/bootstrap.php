<?php

require_once __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__ .'/../');
$dotenv->load();

// Instantiate the app
$app = new \Slim\App(['settings' => require __DIR__ . '/../config/settings.php', 'commands' => [
    'GrabNIP' => \App\Controllers\HomeController::class
]]);

// Set up dependencies
require  __DIR__ . '/container.php';

// Register middleware
require __DIR__ . '/middleware.php';

// Register routes
require __DIR__ . '/routes.php';

return $app;

