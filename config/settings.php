<?php

$settings = [];

// Slim settings
$settings['displayErrorDetails'] = true;
$settings['determineRouteBeforeAppMiddleware'] = true;

// Path settings
$settings['root'] = dirname(__DIR__);
$settings['temp'] = $settings['root'] . '/tmp';
$settings['public'] = $settings['root'] . '/public';

// View settings
$settings['twig'] = [
    'path' => $settings['root'] . '/templates',
    'cache_enabled' => false,
    'cache_path' =>  $settings['temp'] . '/twig-cache'
];

// Database settings
$settings['db']['driver'] = 'mysql';
$settings['db']['host'] = env('DB_HOST');
$settings['db']['username'] = env('DB_USER');
$settings['db']['password'] = env('DB_PASS');
$settings['db']['database'] = env('DB_NAME');
$settings['db']['charset'] = 'utf8';
$settings['db']['collation'] = 'utf8_unicode_ci';
$settings['db']['prefix']	= '';

// Logger settings
$settings['logger'] = [
    'name' => 'app',
    'file' => $settings['temp'] . '/logs/app.log',
    'level' => \Monolog\Logger::ERROR,
];

//telegram
$settings['tg']['tg_key']      = env('TG_KEY');
$settings['tg']['tg_username'] = env('TG_USER');
$settings['tg']['tg_channel']  = env('TG_CHANNEL');
$settings['tg']['dataDir']  = $settings['temp'] . '/data.txt';

return $settings;
