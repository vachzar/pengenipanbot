<?php

declare(strict_types=1);

namespace App\Controllers;

use Slim\Container;

class BaseController
{
    /** @var string $container Slim Container*/
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}
