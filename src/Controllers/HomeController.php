<?php

declare(strict_types=1);

namespace App\Controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends BaseController
{

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /** @param mixed[][] $args Argument */
    public function index(Request $request, Response $response, array $args = []) : void
    {
        //create our pen from Penfactory in DIC
        $pen = $this->container->get('pengenipanModel');

        $pen->index();
    }

    public function command() : void
    {
        //create our pen from Penfactory in DIC
        $pen = $this->container->get('pengenipanModel');

        $pen();
    }

    /** @param mixed[][] $args Argument */
    public function setBot(Request $request, Response $response, array $args = []) : void
    {
        //create our pen from Penfactory in DIC
        $pen = $this->container->get('pengenipanModel');

        $pen->setBot();
    }

    /** @param mixed[][] $args Argument */
    public function unsetBot(Request $request, Response $response, array $args = []) : void
    {
        //create our pen from Penfactory in DIC
        $pen = $this->container->get('pengenipanModel');

        $pen->unsetBot();
    }

    public function hook() : void
    {
        //create our pen from Penfactory in DIC
        $pen = $this->container->get('pengenipanModel');

        $pen->hook();
    }
}
