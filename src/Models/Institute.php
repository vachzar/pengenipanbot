<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    /** @var mixed[][] $fillable Kolom Table */
    protected $fillable = [
        'code',
        'name',
        'lulus',
        'usul',
        'usul_persen',
        'nip',
        'btl',
        'tms',
        'sisa',
        'progress_persen',
        'real_persen',
        'last_update',
    ];
}
