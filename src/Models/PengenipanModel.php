<?php

declare(strict_types=1);

namespace App\Models;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;
use Naucon\File\File;
use Naucon\File\FileReader;
use Naucon\File\FileWriter;
use PHPHtmlParser\Dom;
use const PHP_EOL;
use function count;
use function date;
use function env;
use function json_decode;
use function json_encode;
use function round;
use function sprintf;
use function str_replace;
use function strip_tags;
use function substr;
use function trim;

class PengenipanModel
{
    /** @var string $url2parse Google Docs Url to be parsed */
    protected $url2parse = 'https://docs.google.com/spreadsheets/u/1/d/e/' .
        '2PACX-1vRX03iG9bkgW_zaOdChEqqK1hqgmIAAmEBV35Fk9Dr82rfN2SUpI7EiWAJBE_wZ9s67Wms-5f6947Us/pubhtml';
    /** @var string $tgKey Telegram bot API Key */
    protected $tgKey = '';
    /** @var string $tgKey Telegram bot username */
    protected $tgUsername = '';
    /** @var string $tgChannel Telegram channel to broadcast */
    protected $tgChannel = '';
    /** @var string $dataDir Direktori data untuk menyimpan hasil parsing. */
    protected $dataDir = '';
    /** @var string $logDir Direktori log. */
    protected $logDir = '';
    /** @var string $rawDom Hasil parsing */
    protected $rawDom = '';

    /** @param mixed[][] $config App Config */
    public function __construct(array $config)
    {
        if (empty($config)) {
            die('Telegram Config Not Found');
        }
        $this->tgKey      = $config['tg_key'];
        $this->tgUsername = $config['tg_username'];
        $this->tgChannel  = $config['tg_channel'];
        $this->dataDir    = $config['dataDir'];
        $this->logDir     = $config['logDir'];
    }

    public function index() : void
    {
        $fileObject = new File($this->dataDir);
        if ($fileObject->exists()) {
            $d    = $this->read();
            $data = json_decode($d, true);
            echo sprintf(
                '%s <br>Jumlah Pertek NIP: %s <br>Jumlah Instansi %s',
                $data['title'],
                $data['jml_nip'],
                $data['total_data']
            );
        } else {
            echo 'data tidak ditemukan';
        }
    }

    public function __invoke() : void
    {
        $data       = $this->parse();
        $fileObject = new File($this->dataDir);
        if ($fileObject->exists()) {
            $status = $this->compareDataTxt($data);
            if ($status['update']) {
                $this->write($data);
                $reply_markup = [
                    'inline_keyboard' =>
                    [
                        [
                            [
                                'text' => 'Cek Google Doc',
                                'url' => 'https://goo.gl/XmCy3j',
                            ],
                        ],
                        [
                            [
                                'text' => 'Buka @pengenipan_bot',
                                'url' => 'https://t.me/pengenipan_bot',
                            ],
                        ],
                    ],
                ];

                $this->sendBot2Channel($data['title'] . ($status['info']['title'] ?? '') . PHP_EOL .
                    'Jumlah Pertek NIP: ' . $data['jml_nip'] . ($status['info']['jml_nip'] ?? '') . PHP_EOL .
                    'Jumlah Instansi: ' . $data['total_data'] . ($status['info']['total_data'] ?? '') . PHP_EOL .
                    "\xE2\x9C\x85 = _Terupdate_" . PHP_EOL . PHP_EOL .
                    'add @pengenipan\_bot untuk fungsi lebih advance 
                    (pencarian berdasarkan Nama Instansi)', $reply_markup);
                $newInstitutes = $this->saveData();
                if (count($newInstitutes)) {
                    $text = '*Instansi Baru ' . $data['title'] . '*' . PHP_EOL;
                    foreach ($newInstitutes as $instansi) {
                        $text .= '-  ' . $instansi . PHP_EOL;
                    }
                    $text .= PHP_EOL . 'Gunakan @pengenipan\_bot dan Metode Pencarian: ' .
                             PHP_EOL . '`/cek <nama_instansi>`' .
                             PHP_EOL . 'Untuk melihat detail lengkap tiap instansi';
                    $this->sendBot2Channel($text);
                }

                $this->compareFavInstitute();
            }
        } else {
            $this->write($data);
            $this->sendBot2Channel($data['title'] . PHP_EOL . 'Jumlah Pertek NIP: ' . $data['jml_nip'] . PHP_EOL .
                'Jumlah Instansi: ' . $data['total_data']);
        }
    }

    private function compareFavInstitute() : void
    {
        $text          = '';
        $favInstitutes = FavInstitute::all();
        foreach ($favInstitutes as $favInstitute) {
            $instansi = Institute::where('name', $favInstitute->name)->first();
            $text    .= sprintf(
                '*%s*' . PHP_EOL .
                'Progress Pengusulan:' . PHP_EOL .
                '- Jumlah Lulus: %s' . ($instansi->lulus !== $favInstitute->lulus ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- Usul Masuk: %s' . ($instansi->usul !== $favInstitute->usul ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- Progress: %s' .
                ($instansi->usul_persen !== $favInstitute->usul_persen ? " \xE2\x9C\x85" :'') . PHP_EOL .
                'Progress Penetapan:' . PHP_EOL .
                '- NIP: %s' . ($instansi->nip !== $favInstitute->nip ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- BTL: %s' . ($instansi->btl !== $favInstitute->btl ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- TMS: %s' . ($instansi->tms !== $favInstitute->tms ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- Sisa: %s' . ($instansi->sisa !== $favInstitute->sisa ? " \xE2\x9C\x85" :'') . PHP_EOL .
                '- Progress (versi BKN): %s' .
                ($instansi->progress_persen !== $favInstitute->progress_persen ? " \xE2\x9C\x85" :'') . PHP_EOL .
                'Progress versi Bot (NIP/Usul Masuk)x100 = %s' .
                ($instansi->real_persen !== $favInstitute->real_persen ? " \xE2\x9C\x85" :'') . PHP_EOL .
                'Update: %s' . PHP_EOL . PHP_EOL,
                $instansi->name,
                $instansi->lulus,
                $instansi->usul,
                $instansi->usul_persen . '%',
                $instansi->nip,
                $instansi->btl,
                $instansi->tms,
                $instansi->sisa,
                $instansi->progress_persen . '%',
                $instansi->real_persen . '%',
                $instansi->last_update
            );

            $favInstitute->lulus           = $instansi->lulus;
            $favInstitute->usul            = $instansi->usul;
            $favInstitute->usul_persen     = $instansi->usul_persen;
            $favInstitute->nip             = $instansi->nip;
            $favInstitute->btl             = $instansi->btl;
            $favInstitute->tms             = $instansi->tms;
            $favInstitute->sisa            = $instansi->sisa;
            $favInstitute->progress_persen = $instansi->progress_persen;
            $favInstitute->real_persen     = $instansi->real_persen;
            $favInstitute->save();
        }
        if (empty($text)) {
            return;
        }

        $this->sendBot2Channel($text);
    }

    /** @param mixed[][] $data data hasil parsing
     *
     * @return mixed[][]
     */
    private function compareDataTxt(array $data) : array
    {
        $d                = $this->read();
        $pengenipan       = json_decode($d, true);
        $status['update'] = false;
        $whatUpdated      = ['title' => false, 'jml_nip' => false, 'total_data' => false];

        if ($pengenipan['title'] !== $data['title']) {
            $whatUpdated['title'] = "\xE2\x9C\x85";
            $status['update']     = true;
        }

        if ($pengenipan['jml_nip'] !== $data['jml_nip']) {
            $whatUpdated['jml_nip'] = "\xE2\x9C\x85";
            $status['update']       = true;
        }

        if ($pengenipan['total_data'] !== $data['total_data']) {
            $whatUpdated['total_data'] = "\xE2\x9C\x85";
            $status['update']          = true;
        }

        $status['info'] = $whatUpdated;

        return $status;
    }

    /**
     * @return mixed[][]
     */
    private function parse() : array
    {
        $data = [];
        $dom  = new Dom();
        $dom->loadFromUrl($this->url2parse);

        $b             = $dom->find('#581113312R1')[0];
        $sibling       = $b->nextSibling();
        $data['title'] = trim(strip_tags($sibling->innerHtml));

        $c               = $dom->find('#581113312R2')[0];
        $jml             = $c->nextSibling()->nextSibling();
        $data['jml_nip'] = trim(str_replace('.', '', $jml->innerHtml));

        $a                  = $dom->find('table.waffle tbody')[0];
        $last_child         = $a->lastChild();
        $last_num           = $last_child->firstChild()->nextSibling();
        $data['total_data'] = $last_num->innerHtml;

        $data['last_check'] = date('Y-m-d H:i:s');

        $this->rawDom = $a;

        return $data;
    }

    /**
     * @return mixed[][]
     */
    private function saveData() : array
    {
        $cols         = [
            'code',
            'name',
            'lulus',
            'usul',
            'usul_persen',
            'nip',
            'btl',
            'tms',
            'sisa',
            'progress_persen',
            'last_update',
        ];
        $contents     = $this->rawDom;
        $newInstitute = [];
        foreach ($contents as $content) { // get the class attr
            $id = $content->firstChild()->getAttribute('id');
            if (! $id) {
                continue;
            }
            $id = trim(substr($id, 10));
            if ($id < 6) {
                continue;
            }
            $anak = [];
            $i    = -1;
            foreach ($content as $child) {
                if ($i === -1) {
                    $i++;
                    continue;
                }

                if ($i>11) {
                    continue;
                }

                $anak[$cols[$i++]] = str_replace('%', '', trim(strip_tags($child->innerHtml)));
            }
            $institute = Institute::updateOrCreate(
                ['name' => $anak['name']],
                $anak
            );
            if ($institute->wasRecentlyCreated === true) {
                $newInstitute[] = $institute->name;
            }
            $institute->real_persen = round(($institute->nip / $institute->usul) * 100, 2);
            $institute->save();
        }

        return $newInstitute;
    }

     /**
      * @return object
      */
    private function read() : string
    {
        $fileObject = new FileReader($this->dataDir, 'r', true);

        return $fileObject->firstLine();
    }

    /**
     * @param mixed[][] $data
     */
    private function write(array $data) : void
    {
        $fileObject = new FileWriter($this->dataDir, 'w+');
        $fileObject->write(json_encode($data));
    }

    /**
     * @param bool|mixed[][] $reply_markup
     */
    private function sendBot2Channel(string $text, $reply_markup = false) : void
    {
        $bot_api_key  = $this->tgKey;
        $bot_username = $this->tgUsername;

        try {
            $telegram = new Telegram($bot_api_key, $bot_username);
            TelegramLog::initErrorLog($this->logDir . '/' . $bot_username . '_error.log');
            TelegramLog::initDebugLog($this->logDir . '/' . $bot_username . '_debug.log');
            TelegramLog::initUpdateLog($this->logDir . '/' . $bot_username . '_update.log');
            $telegram->enableLimiter();

            $data = [
                'chat_id' => $this->tgChannel,
                'text'    => $text,
                'parse_mode' => 'markdown',
            ];

            if ($reply_markup) {
                $data['reply_markup'] = $reply_markup;
            }

            $result = Request::sendMessage($data);

            //var_dump($result);
        } catch (TelegramException $e) {
            // Silence is golden!
            //echo $e;
            // Log telegram errors
            TelegramLog::error($e);
        } catch (TelegramLogException $e) {
            // Silence is golden!
            // Uncomment this to catch log initialisation errors
            //echo $e;
        }
    }

    public function unsetBot() : void
    {
        try {
            // Create Telegram API object
            $telegram = new Telegram($this->tgKey, $this->tgUsername);
            // Delete webhook
            $result = $telegram->deleteWebhook();
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (TelegramException $e) {
            echo $e->getMessage();
        }
    }

    public function setBot() : void
    {
        $hook_url = env('SITE_URL') . '/hook';

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->tgKey, $this->tgUsername);

            // Set webhook
            $result = $telegram->setWebhook($hook_url);
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (TelegramException $e) {
            // log telegram errors
            // echo $e->getMessage();
        }
    }

    public function hook() : void
    {
        $commands_paths = [
            __DIR__ . '/../Commands/',
        ];

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->tgKey, $this->tgUsername);

            // Add commands paths containing your custom commands
            $telegram->addCommandsPaths($commands_paths);

            // Enable admin users
            // $telegram->enableAdmins($admin_users);

            // Logging (Error, Debug and Raw Updates)
            TelegramLog::initErrorLog($this->logDir . '/' . $this->tgUsername . '_error.log');
            //\Longman\TelegramBot\TelegramLog::initDebugLog($this->logDir . "/{$this->tgUsername}_debug.log");
            //\Longman\TelegramBot\TelegramLog::initUpdateLog($this->logDir . "/{$this->tgUsername}_update.log");

            // Set custom Upload and Download paths
            $telegram->setDownloadPath(__DIR__ . '/../../tmp/download');
            $telegram->setUploadPath(__DIR__ . '/../../tmp/upload');

            // Requests Limiter (tries to prevent reaching Telegram API limits)
            $telegram->enableLimiter();

            // Handle telegram webhook request
            $telegram->handle();
        } catch (TelegramException $e) {
            // Silence is golden!
            // log telegram errors
            // echo $e->getMessage();
            TelegramLog::error($e);
        } catch (TelegramLogException $e) {
            // Silence is golden!
            // Uncomment this to catch log initialisation errors
            TelegramLog::error($e);
        }
    }
}
