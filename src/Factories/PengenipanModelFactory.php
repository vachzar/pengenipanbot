<?php

declare(strict_types=1);

namespace App\Factories;

use App\Models\PengenipanModel;
use Slim\Container;

class PengenipanModelFactory
{
    /** @var string $container Slim Container*/
    protected $container;

    public function __invoke(Container $container) : PengenipanModel
    {
        $this->container  = $container;
        $config           = $container['settings']['tg'];
        $config['logDir'] = $container['settings']['temp'] . '/logs';

        return new PengenipanModel($config);
    }
}
