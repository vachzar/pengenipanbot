<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use App\Models\Institute;
class CekCommand extends UserCommand
{
    protected $name = 'cek';                      // Your command's name
    protected $description = 'Untuk mencari Informasi Instansi'; // Your command description
    protected $usage = '/cek <nama instansi>';                    // Usage of your command
    protected $version = '1.0.0';                  // Version of your command

    public function execute()
    {
        $message = $this->getMessage();            // Get Message object

        $chat_id = $message->getChat()->getId();   // Get the current Chat ID
        $search_str = trim($message->getText(true));
        $data = [
            'chat_id'    => $chat_id,
            'parse_mode' => 'markdown',
        ];
        if ($search_str === '') {
            $data['text'] = 'Gunakan /cek <nama instansi> untuk pencarian' . PHP_EOL;
            $data['text'] .= 'Contoh: /cek Jawa Timur';
            return Request::sendMessage($data);
        }

        if (strlen($search_str) < 4) {
            $data['text'] = 'Untuk Pencarian Minimal 4 Karakter' . PHP_EOL;
            $data['text'] .= 'Contoh: /cek Jawa Timur';
            return Request::sendMessage($data);
        }

        $institutes = Institute::Where('name', 'like', '%' . $this->escape_like($search_str) . '%')->get();
        if($jml = count($institutes)){
            $data['text'] = "*$jml Instansi Ditemukan*:" . PHP_EOL;
            $i = 1;
            foreach ($institutes as $instansi) {
                $data['text'] .= sprintf(
                    '*%s*' . PHP_EOL .
                    'Progress Pengusulan:' . PHP_EOL .
                    '- Jumlah Lulus: %s'. PHP_EOL .
                    '- Usul Masuk: %s'. PHP_EOL .
                    '- Progress: %s'. PHP_EOL .
                    'Progress Penetapan:'. PHP_EOL .
                    '- NIP: %s'. PHP_EOL .
                    '- BTL: %s'. PHP_EOL .
                    '- TMS: %s'. PHP_EOL .
                    '- Sisa: %s'. PHP_EOL .
                    '- Progress (versi BKN): %s'. PHP_EOL .
                    'Progress versi Bot (NIP/Usul Masuk)x100 = %s'. PHP_EOL .
                    'Update: %s'. PHP_EOL. PHP_EOL,
                    $instansi->name,
                    $instansi->lulus,
                    $instansi->usul,
                    $instansi->usul_persen.'%',
                    $instansi->nip,
                    $instansi->btl,
                    $instansi->tms,
                    $instansi->sisa,
                    $instansi->progress_persen.'%',
                    $instansi->real_persen.'%',
                    $instansi->last_update
                );
                if(12 < $i++){
                    $data['text'] .= 'Hasil Pencarian Dipotong, perluas keyword agar mendapatkan hasil yang lebih rinci.';
                    break;
                }
            }
        }else{
            $data['text'] = 'Instansi tidak ditemukan, gunakan keyword lain.';
        }
        return Request::sendMessage($data);        // Send message!
    }
    function escape_like(string $value, string $char = '\\'): string
    {
        return str_replace(
            [$char, '%', '_'],
            [$char.$char, $char.'%', $char.'_'],
            $value
        );
    }
}
