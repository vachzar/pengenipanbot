# Latar belakang
BKN mengupdate progress pengeNIPan CPNS 2018 melalui laman [Google Drive](https://goo.gl/XmCy3j), biasanya mereka juga men-tweet jika ada update.
Karena saya terlalu malas untuk selalu mengecek twitter ataupun halaman Google Drive, maka terciptalah bot ini.
Apps ini mengecek apakah ada update di Google Drive (tanggal, jumlah NIP, jumlah Instansi) lalu dengan bot @pengenipan_bot mengirimkan pesan progress ke channel Telegram [@pengenipan](https://t.me/pengenipan).

## Feature
*  automatically checks for data changes
*  search progress by Instansi (ex: `/cek Jawa Timur`)

## Requirement
*  PHP 7.0+
*  MySQL 5.7+
  
## How to Install
I intentionally pushing vendor folder since my shared hosting hasn't support Composer. So just clone this repo.
*  create Telegram Bot from [BotFather](https://telegram.me/botfather) copy TOKEN Access HTTP API
*  create channel (for auto-broadcasting the updates)
*  set your bot as admin on your channel
*  create db and import db.sql
*  copy .env.example to .env 
*  edit .env value to your own settings
*  set cronjob (I set mine to every 5 min) `*/5 * * * * /usr/local/bin/php /your/project/location/pengenipan/public/index.php GrabNIP 2>&1`
*  set your webhook in .env (must be https://) and then visit https://yoursite.com/set
*  ...
*  ...
*  profit


## Next to do (kalau sempat)
dan proses pengeNIPannya gak keburu selesai
 - Suggest me
 - 

## Credit
*  [Slim Framework](http://www.slimframework.com)
*  [php-telegram-bot](https://github.com/php-telegram-bot/core#projects-with-this-library)
*  [First Slim 3 Tutorial](https://odan.github.io/2017/11/30/creating-your-first-slim-framework-application.html), Official tutorial is sucks :p
*  [Slim 3 with Eloquent ORM Tutorial](https://www.cloudways.com/blog/using-eloquent-orm-with-slim/)
*  [Emoji Unicode Tables](https://apps.timwhitlock.info/emoji/tables/unicode)
*  BKN