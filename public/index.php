<?php
date_default_timezone_set('Asia/Jakarta');
/** @var Slim\App $app */
$app = require __DIR__ . '/../config/bootstrap.php';

// Start
$app->run();

